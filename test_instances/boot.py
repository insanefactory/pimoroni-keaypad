import board
from digitalio import Direction, DigitalInOut, Pull
import storage
import usb_cdc


usb_cdc.enable(console=True, data=True)    # Enable console and data
print("Consoles enabled...")


switch = DigitalInOut(board.GP16)
switch.direction = Direction.INPUT
switch.pull = Pull.UP
if switch.value:
    storage.remount("/", False)
    storage.disable_usb_drive()
    print("Mass storage disabled...")

