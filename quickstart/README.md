# Starting with InSaneFactory on Pimoroni Pico RGB Keypad
0) Start with downloading and installing [Circuit python](https://circuitpython.org/board/raspberry_pi_pico/) on yout RPi Pico.
0) Download and add [PMK library](https://github.com/pimoroni/pmk-circuitpython/tree/main/lib) to your Pico. 
0) Donwload and add [Adafruit DotStar library](https://github.com/adafruit/Adafruit_CircuitPython_DotStar). It's required by PMK.
0) Now you can start with ISF. Head to [main InSaneFactory project](https://gitlab.com/insanefactory/main/-/tree/modules). You want to donwload [`code.py`](https://gitlab.com/insanefactory/main/-/blob/modules/code.py) and copy it as you main script.
0) Next in the `lib` folder find `in_sane_factory` and copy it in `lib` folder on your Pi.
0) You probably want copy `isf_hid` as well. This library gives you pover to use your device as keyboard. Requiremet of this `isf_hid` is [Adafruit HID library](https://github.com/adafruit/Adafruit_CircuitPython_HID). You can download it fresh new or (for now) it's present in out repository.
0) Back in this repository you want to grap `boot.py`, `config.json` and `resources.json`. 
    * `booty.py` runs before your main code starts and this one opens second serial interface that can be used to comunication with desktop app or other software. 
    * `resources.json` contain all hardware setting. For us none are needed, so this file stays as empty dict ( `{}` ).
    * `config.json` This is our file of interest. You can find there basic cofiguration that may help you understand how system works. Hopefully this file is the only you need to edit. For more information go to [main repository](https://gitlab.com/insanefactory/main/-/tree/modules).
0) If you did not, unplug and plug device to fully restart it. Now you may see USB drive and two serail ports.
0) Last but important step is opening your serial inreface. Usualy serial ports are open in series, that the lover index is you python (debug) interface and the higher is your data interface. When you create or edit config it's good idea to open your python port and watch how ISF react to changes and if it loaded properly. 
0) You are ready to explore! If everything goes as planed, your Keypad may looks something like this:
![Ilustrational photo.](Image.jpeg)
Four buttons in the middle can now perfor actions defined in the `congif.json`. Go ahead, try them and make it your own. Have a fun.
> Note: Fun is not required.
